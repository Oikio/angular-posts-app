'use strict';

import {Component, EventEmitter} from '@angular/core';

@Component({
  selector: 'posts-list',
  template: `
<div class="posts-list__el"
     *ngFor="let post of posts"
     [ngClass]="{_active: postIsSelected(post)}"
     (click)="selectPost(post)">
    {{post.title}}
    <div (click)="makeFavorite(post, $event)" [ngClass]="{_active: post.favorite}" class="post-list__favorite fa fa-heart"></div>
</div>
`,
  inputs: ['posts', 'selectedPost'],
  outputs: ['selectedPostChange']
})
export default class PostsList {
  constructor() {
    this.selectedPostChange = new EventEmitter();
  }

  selectPost(post) {
    this.selectedPost = post;
    this.selectedPostChange.emit(post)
  }

  postIsSelected(post) {
    return post === this.selectedPost;
  }

  makeFavorite(post, $event) {
    $event.stopPropagation();
    post.favorite = true;
  }

}