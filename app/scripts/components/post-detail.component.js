import {Component} from '@angular/core';


@Component({
  selector: 'post-detail',
  template: `
<h2 class="post-detail__heading">{{post.title}}</h2>
<div class="post-detail__body">{{post.body}}</div>

`,
  inputs: ['post']
})
export default class PostDetail {
  constructor() {}
}