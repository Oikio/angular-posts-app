'use strict';

import {Component} from '@angular/core';
import {NgClass} from '@angular/common';
import PostService from './services/post.service';

@Component({
  selector: 'app',
  template: `
<div class="app">
    <header>
        <h1 class="app-header">Blog Post Reader</h1>
    </header>
    <main class="app__inner">
        <posts-list class="posts-list" [posts]="posts" [(selectedPost)]="selectedPost"></posts-list>
        <post-detail class="post-detail" *ngIf="selectedPost" [post]="selectedPost"></post-detail>
    </main>
</div>
`,
  providers: [PostService]
})
export default class AppComponent {
  constructor(postService) {
    this.postService = postService;

    this.title = 'Blog Post Reader';
    this.selectedPost = null;
  }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    return this.postService.getPosts().then(posts => this.posts = posts);
  }
}

AppComponent.parameters = [PostService];