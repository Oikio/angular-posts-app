import {Injectable} from '@angular/core';
import {Http, Headers}    from '@angular/http'
import Post from '../models/post.model';
import 'rxjs/add/operator/toPromise';

@Injectable()
export default class PostService {

  constructor(http) {
    this.http = http;
  }

  getPosts() {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http
      // .get('http://jsonplaceholder.typicode.com/posts')
      .get('http://www.mocky.io/v2/57d9554b0f0000822583159d', {headers: headers})
      .toPromise()
      .then(response => {
        return response.json().map(p => new Post(p))
      });
  }
}

PostService.parameters = [Http]