'use strict';

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule}    from '@angular/http'

import AppComponent from './app.component';
import PostsList from './components/posts-list.component';
import PostDetail from './components/post-detail.component';

@NgModule({
  imports: [BrowserModule, HttpModule],
  declarations: [AppComponent, PostsList, PostDetail],
  bootstrap: [AppComponent]
})
export default class AppModule {
  constructor() {}
}