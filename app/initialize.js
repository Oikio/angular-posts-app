import 'zone.js';
import 'reflect-metadata';


import AppModule from './scripts/app.module';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';


document.addEventListener('DOMContentLoaded', function () {
  platformBrowserDynamic().bootstrapModule(AppModule);
});