
OMG how bloated Angular is. I spent about 5 hours for pure documentation reading and about 5-6 hours on coding with docs opened in hundreds of tabs =). 
My past small experience gave me almost zero boost.

I met a lot of problems regarding Angular with ES2015 usage, some docs tell you different things and almost all examples are in typescript only.
A lot of time I spent searching how you should solve it with angular on javascript instead of typescript.

If you look at post.service.js - you will find out, that I could not retrieve posts from your endpoint because of CORS policy:
`XMLHttpRequest cannot load http://jsonplaceholder.typicode.com/posts. No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'http://localhost:3333' is therefore not allowed access. The response had HTTP status code 403.`  
If it's part of your puzzle, I did not solve it =). So I used online tool to make a response, link might expire when you run app, so just uncomment your endpoint.

So here comes the worst piece of angular code you have seen:

# Build
 * Tried Angular-CLI at first, but it's so unfinished and also I has no experience with Broccoli configure it 
 * Used brunch (great tool, by the way) as build tool and skeleton, description how to run it is in the end of readme file
 * Structure is somewhat Angular conventions told me to do

# Javascript
 * I used decorators in code to simplify code for myself, I would not use them on production, they are not production ready and spec also might change.
 As you know, Angular uses decorator on type definitions, so not all decorators are suitable for js code
 * Decided to use ES2015 classes - it was not so easy, because there are no docs in the wild regarding usage of them with Angular and it's decorators   
 * I used string templates to simplify build
 * No Angular production mode used, not minification and other build related stuff done, I suppose it's not part of the task
 * I wanted to add router, but had no time for it
 
### Confusions
 * Docs, it was not that easy to find answers
 * response.json method returns JSON instead of Promise like in fetch API
 * I'm not sure regarding DI if I done it right
 * To allow .toPromise method for HTTP you have to import Rx, that nice one =)
 * I did not find good way to separate model and view-model, where I would like to keep post's 'favorite' state, so I used model itself 
 
# Styles 
 Just plain css, no preprocessors or vendor prefixes. I decided to omit postCSS and SASS, as it is not part of task.
 I do not use flexbox on work because of legacy browsers, so I had to remember how shrink and grow works, took some time.
 
Angular is in beta and while I was looking answers on SO, I saw lot of misunderstanding and problems people get into with it.
Also github show a lot of opened issues for project which are not get answered fast enough and as I mentioned angular-cli is unfinished. 
It's not part of my task, but IMHO Angular is really not production ready by all means. 
Maybe just in case if you care about Ionic and team really loves it and had experience with first Angular attempt, which they dropped.


# Running app

* Install (if you don't have them):
    * [Node.js](http://nodejs.org): `brew install node` on OS X
    * [Brunch](http://brunch.io): `npm install -g brunch`
    * Brunch plugins and app dependencies: `npm install`
* Run:
    * `brunch watch --server` — watches the project with continuous rebuild. This will also launch HTTP server with [pushState](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Manipulating_the_browser_history).
    * `brunch build --production` — builds minified project for production